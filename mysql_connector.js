const mysql = require("mysql2/promise");
const { Connector } = require("@google-cloud/cloud-sql-connector");
class MySQLConnector {
  constructor(configOptions) {
    this.configOptions = configOptions;
  }

  async connection() {
    this.connector = new Connector();

    this.clientOpts = await this.connector.getOptions({
      instanceConnectionName: "cellular-client-400009:us-central1:bot-api",
      ipType: "PUBLIC",
    });
    console.log(this.clientOpts);
    this.pool = mysql.createPool({ ...this.configOptions, ...this.clientOpts });
  }

  async close() {
    //await this.pool.end();

    this.connector.close();
  }

  async reconnect() {
    await this.close();
    await this.connection();
  }

  incrementBE(buffer) {
    for (var i = buffer.length - 1; i >= 0; i--) {
      if (buffer[i]++ !== 255) break;
    }
    return buffer;
  }

  async getDocsByFields(sqlString) {
    const conn = await this.pool.getConnection();
    //console.log(conn);
    const [result] = await conn.query(sqlString);
    console.log(result[0]["doc"]);
    await conn.close();

    return result.map((doc) => {
      return doc.doc;
    });
  }

  async getDocByFields(sqlString) {
    const result = await this.getDocsByFields(sqlString);
    return result[0];
  }

  async getDocsById(id, collectionName) {
    const conn = await this.pool.getConnection();
    const [result] = await conn.query(
      `SELECT * FROM ${collectionName} WHERE JSON_EXTRACT(doc ,'$._id') =  '${id}';`
    );
    console.log(result);
    console.log(result[0]._id.toString("hex"));
    await conn.close();
    return result.map((doc) => {
      return doc.doc;
    });
  }

  async getDocById(id, collectionName) {
    const result = await this.getDocsById(id, collectionName);
    return result[0];
  }

  async getRows(sqlString) {
    const conn = await this.pool.getConnection();
    const [result] = await conn.query(sqlString);
    await conn.close();
    return result;
  }

  async getRow(sqlString) {
    const result = await this.getRows(sqlString);
    return result[0];
  }

  async insertDocs(collectionName, docs) {
    const conn = await this.pool.getConnection();
    const [last] = await conn.query(
      "select * from tips order by _id DESC limit 1;"
    );
    console.log(last);
    const prefix = last[0]._id.slice(0, 4);
    const timestamp = Math.round(now.getTime() / 1000).toString(16);
    const new_id = (parseInt(last[0]._id.slice(13, 28), 16) + 1).toString(16);
    const padding = last[0]._id.slice(13, 28 - new_id.length);
    console.log(`${prefix}${timestamp}${padding}${new_id}`);
    for (const doc of docs) {
      const now = new Date();
      const [result] = await conn.query(
        `insert into ${collectionName} (doc) values ('${JSON.stringify({
          ...doc,
          _id: prefix + timestamp + padding + new_id,
          createdAt: Math.round(now.getTime() / 1000),
          lastUpdatedAt: Math.round(now.getTime() / 1000),
        })}');`
      );
      console.log(result);
    }
    await conn.close();
  }

  async insertRows(headers, tableName, rowsArray) {
    const conn = await this.pool.getConnection();
    for (const row of rowsArray) {
      let rowString = "";
      for (const column of row) {
        if (typeof column == "string") {
          rowString = `${rowString} '${column}',`;
        } else if (typeof column == "boolean") {
          rowString = `${rowString} ${column},`;
        } else {
          rowString = `${rowString} ${column},`;
        }
      }

      const now = new Date();
      const [result] = await conn.query(
        `insert into ${tableName} (${headers.join(
          ", "
        )}, lastUpdated) values (${rowString} ${Math.round(
          now.getTime() / 1000
        )})`
      );
      console.log(result);
    }
    await conn.close();
  }

  async updateRows(tableName, updateObjs, whereString) {
    const conn = await this.pool.getConnection();
    for (const row of updateObjs) {
      let setString = "";
      Object.entries(row).forEach(([key, value]) => {
        if (typeof value == "string") {
          setString = `${setString} ${key} = '${value}',`;
        } else if (typeof column == "boolean") {
          setString = `${setString} ${key} = ${value},`;
        } else {
          setString = `${setString} ${key} = ${value},`;
        }
      });
      const now = new Date();
      const [result] = await conn.query(
        `UPDATE ${tableName} SET ${setString} lastUpdated = ${Math.round(
          now.getTime() / 1000
        )} where ${whereString}`
      );
      console.log(result);
    }
    await conn.close();
  }

  async updateDocs(collectionName, updateObjs, whereString) {
    const conn = await this.pool.getConnection();
    for (const row of updateObjs) {
      let [result] = await conn.query(
        `select * from ${collectionName} where ${whereString}`
      );
      console.log(result);
      const oldDoc = result[0].doc;
      const newDoc = {
        ...oldDoc,
        ...row,
        lastUpdated: Math.round(now.getTime() / 1000),
      };
      console.log(newDoc);
      let [updated] = await conn.query(
        `UPDATE ${collectionName} SET doc = '${JSON.stringify(
          newDoc
        )}' where ${whereString}`
      );
      console.log(updated);
    }
    await conn.close();
  }
}

module.exports = MySQLConnector;
