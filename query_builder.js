const { toTimestamp } = require("../../node_normalization/numbers");
const IP = require("ip");
const upload_error = require("../../node_error_functions/upload_error");

module.exports = async (source, queryType, data, table, key, value) => {
  let q;
  console.log(source);
  try {
    if (queryType === "update") {
      console.log(key, value);
      console.log(data);

      q = await q.where(`${key} = :${key}`).bind(`${key}`, value).execute();
      let set_string = "";
      for (const key in data) {
        fields = `${fields} ${key},`;
        if (typeof data[key] == "number") {
          set_string = `${set_string} ${key} = ${data[key]},`;
        } else {
          set_string = `${set_string} ${key} = "${data[key]}",`;
        }
      }
      let SQL = `INSERT INTO ${table} SET ${set_string} lastUpdated)
        = ${toTimestamp(new Date())};`;
      console.log(SQL);

      let [newDoc] = await source.query(SQL).execute();
      console.log(newDoc);
      let docId = newDoc.getAutoIncrementValue();

      return docId;
    } else if (queryType === "insert") {
      let fields = ``;
      let values = ``;
      console.log(data);
      for (const key in data) {
        fields = `${fields} ${key},`;
        if (typeof data[key] == "number") {
          values = `${values} ${data[key]},`;
        } else {
          values = `${values} "${data[key]}",`;
        }
      }
      let SQL = `INSERT INTO ${table} (${fields} lastUpdated)
        VALUES (${values} ${toTimestamp(new Date())});`;
      console.log(SQL);
      let [newDoc] = await source.query(SQL).execute();
      console.log(newDoc);

      let docId = newDoc.getAutoIncrementValue();

      return docId;
    }
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "GError building SQL Query",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
