const mysql = require("mysql2/promise");
const { loader } = require("../../node_function_loader/loader");

const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../env.js");
const { Connector } = require("@google-cloud/cloud-sql-connector");
const connector = new Connector();
let connection = async () => {
  
  const clientOpts = await connector.getOptions({
    instanceConnectionName: "cellular-client-400009:us-central1:bot-api",
    ipType: "PUBLIC",
  });
  const pool = await mysql.createPool({
    ...clientOpts,
    user: MYSQL_USER,
    password: MYSQL_PASS,
    database: MYSQL_DATABASE,
  });

  return pool;
};

const client = connection();

const initDB = async () => {
  // connect to sql server

  // initate session
  const mySession = await client.getConnecton();

  console.log(mySession);

  // load table sync functions
  const tableObject = loader(__dirname.replace("mysql", "tables"));

  // sync database
  for (const key in tableObject) {
    if (tableObject.hasOwnProperty(key)) {
      tableObject[key](mySession, mySession);
    }
  }
};

const reconnect = async () => {
  await client.end();
  connector.close()
  connector = new Connector();
  const clientOpts = await connector.getOptions({
    instanceConnectionName: "cellular-client-400009:us-central1:bot-api",
    ipType: "PUBLIC",
  });
  client = await mysql.createPool({
    ...clientOpts,
    user: MYSQL_USER,
    password: MYSQL_PASS,
    database: MYSQL_DATABASE,
  });

  return client;
};
module.exports.initDB = initDB;
module.exports.reconnect = reconnect;
module.exports.client = client;
